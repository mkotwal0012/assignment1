This application was created by the Conestoga Capstone Team enrolled for Computer Application Development Program. 
The data of this application is essentially pulled from the Google API.

Feature:
- The Application allows you to book online movie tickets at the nearest Theatre location.
- The Application lets you to select the available seats as per your choice.
- The system auto-generated e-ticket and also an online payment reciept which is emailed to the customer.
- The system has an optional feature of having online food coupons

Steps to use the Application:
To run this app, clone this repo to your local machine. Then click on the .exe file to run the app.

Primative Environment:
Node.Js/Express
Visual Studio Code
MongoDB
GitHUB
HTML/CSS
Windows OS

Note: MIT License allows for commercial use, modifications, distribution and private use. Hence, this repo is copyrighted under MIT License.
Edit: An article is added to the wiki that discusses repo privacy and open-source software.